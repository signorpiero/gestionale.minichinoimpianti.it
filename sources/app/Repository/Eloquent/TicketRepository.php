<?php

namespace App\Repository\Eloquent;

use App\Ticket;
use App\Repository\TicketRepositoryInterface;
use Illuminate\Support\Collection;

class TicketRepository extends BaseRepository implements TicketRepositoryInterface
{

   /**
    * TicketRepository constructor.
    *
    * @param Ticket $model
    */
   public function __construct(Ticket $model)
   {
       parent::__construct($model);
   }

   public function paginate($perPage = 15)
   {
       return $this->model->paginate($perPage);
   }
   public function getTicketsPaginatedForUserRole($user,$perPage)
   {
       if($user->role->id == 1)
       {
        return $this->model->orderBy('created_at','desc')->with('user')->paginate($perPage);
       }
       return $this->model->where('user_id',$user->id)->orderBy('created_at','desc')->with('user')->paginate($perPage);
   }
}
