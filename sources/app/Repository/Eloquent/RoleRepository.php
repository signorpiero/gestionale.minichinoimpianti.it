<?php

namespace App\Repository\Eloquent;

use App\Role;
use App\Repository\RoleRepositoryInterface;

class RoleRepository extends BaseRepository implements RoleRepositoryInterface
{

   /**
    * RoleRepository constructor.
    *
    * @param Role $model
    */
   public function __construct(Role $model)
   {
       parent::__construct($model);
   }

}