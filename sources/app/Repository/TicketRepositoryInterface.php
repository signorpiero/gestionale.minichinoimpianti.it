<?php

namespace App\Repository;
use Illuminate\Support\Collection;

interface TicketRepositoryInterface
{
    public function paginate($perPage = 15);
    public function getTicketsPaginatedForUserRole($role,$perPage);
}