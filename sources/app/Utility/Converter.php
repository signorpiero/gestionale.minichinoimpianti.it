<?php   

namespace App\Utility;

use Illuminate\Support\Carbon;

class Converter{

    static function toItalianDayDate($dayDate){
        return Carbon::parse($dayDate)->format('d-m-Y H:i');
    }
}