<?php

namespace App\Http\Controllers;

use App\Events\TicketSended;
use App\Ticket;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repository\TicketRepositoryInterface;

class TicketController extends Controller
{
    private $ticketRepository;
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(TicketRepositoryInterface $ticketRepository)
    {
        $this->middleware('auth');
        //$this->middleware('role:2');
        $this->ticketRepository = $ticketRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$tickets = $this->ticketRepository->getTicketsPaginatedForUserRole(Auth::user(),10);

        return view("ticket.index");
    }

    public function getTickets()
    {
        $perPage = 5;
        //$tickets = $this->ticketRepository->getTicketsPaginatedForUserRole(Auth::user(),10);

        if(Auth::user()->role->id == 1)
        {
            return response()->json(Ticket::with('user')->orderBy('created_at','desc')->paginate($perPage));
        }
        return response()->json(Ticket::with('user')->where('user_id',Auth::user()->id)->orderBy('created_at','desc')->paginate($perPage));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("ticket.create");
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'text' => 'required'
        ]);

        $ticket = Auth::user()->tickets()->create(['text' => $request['text']]);

        if(!$ticket){
            return view("ticket.create",['error' => 'Error']);
        }
        broadcast(new TicketSended($ticket));
        return view("ticket.create",['success' => 'Success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
