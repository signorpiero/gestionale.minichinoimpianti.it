<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;

class CheckUserRole
{
    protected $redirectTo = RouteServiceProvider::HOME;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if ($request->user()->role->id != $role) {
            return redirect('errore');
        }
        return $next($request);
    }
}
