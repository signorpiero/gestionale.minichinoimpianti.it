<?php

namespace App;

use App\Utility\Converter;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'text'
    ];
    //
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getCreatedAtAttribute($value)
    {
        return Converter::toItalianDayDate($value);
    }
}
