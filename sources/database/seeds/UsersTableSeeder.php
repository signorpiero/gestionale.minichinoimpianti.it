<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => "Amministratore",
                'username' => "admin",
                'email' => "admin@admin.com",
                'password'  =>  bcrypt("admin123"),
                'role_id'   =>  1
            ],
            [
                'name' => "Cliente",
                'username' => "cliente",
                'email' => "cliente@cliente.com",
                'password'  =>  bcrypt("cliente123"),
                'role_id'   =>  2
            ]
        ]);
    }
}
