<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name' => "Amministratore",
                'description' => "Utente Amministratore del sistema",
                'base_path' => "tickets"
            ],
            [
                'name' => "Cliente",
                'description' => "Cliente",
                'base_path' => "tickets/create"
            ]
        ]);
    }
}
