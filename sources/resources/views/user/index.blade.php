@extends('layouts.app')

@section('content')
<div class="container">
    <users :user="{{ Auth::user() }}" :roles="{{ $roles }}"></users>
    <div class="d-flex justify-content-end">
    </div>
</div>
@endsection
