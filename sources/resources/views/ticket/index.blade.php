@extends('layouts.app')

@section('content')
<div class="container">
    <tickets :user="{{ Auth::user() }}"></tickets>
    <div class="d-flex justify-content-end">
    </div>
</div>
@endsection
