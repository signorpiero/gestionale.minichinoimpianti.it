@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            @isset($error)
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{ __('error.alert') }}</strong> {{ __('error.generic') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endisset
            @isset($success)
                <div class="alert alert-success" role="alert">
                    Ti ringraziamo per la segnalazione, a breve verrà presa in carico.
                </div>
            @endisset
            <div class="card">
                <div class="card-header">{{ __('ticket.new-ticket') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('tickets.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('auth.email') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ Auth::user()->email }}" required autocomplete="email" disabled>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="text" class="col-md-4 col-form-label text-md-right">{{ __('ticket.text') }}</label>
                            <div class="col-md-6">
                                <textarea name="text" class="form-control @error('text') is-invalid @enderror" id="text" rows="5"></textarea>
                                @error('text')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror()
                            </div>
                            
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-10 d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('ticket.send') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
